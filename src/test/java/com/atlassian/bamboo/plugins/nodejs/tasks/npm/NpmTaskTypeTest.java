package com.atlassian.bamboo.plugins.nodejs.tasks.npm;

import com.google.common.collect.Maps;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.Map;

import static com.atlassian.bamboo.plugins.nodejs.tasks.node.NodeCapabilityDefaultsHelper.NPM_EXECUTABLE_NAME;
import static com.atlassian.bamboo.plugins.nodejs.tasks.npm.NpmTaskType.BUILD_CTX_NPM_ISOLATED_CACHE_DIRECTORY;
import static com.atlassian.bamboo.testutils.matchers.FileMatcher.exists;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasKey;

@RunWith(JUnitParamsRunner.class)
public class NpmTaskTypeTest
{
    @Test
    @Parameters
    public void testConvertNodePathToNpmPath(@NotNull final String nodePath, @NotNull final String expectedNpmPath)
    {
        assertThat("Should properly convert Node.js executable path to npm executable path",
                NpmTaskType.convertNodePathToNpmPath(nodePath), equalTo(expectedNpmPath));
    }

    @Test
    public void testTemporaryCacheDirectory() throws Exception
    {
        final Map<String, String> customBuildData = Maps.newHashMap();

        // get (create) cache directory
        final String cacheDirectoryPath = NpmTaskType.getTemporaryCacheDirectory(customBuildData);
        assertThat("Should have saved cache directory path in custom build data",
                customBuildData, hasKey(BUILD_CTX_NPM_ISOLATED_CACHE_DIRECTORY));
        assertThat("Method should have returned the same path that was saved in custom build data",
                cacheDirectoryPath, equalTo(customBuildData.get(BUILD_CTX_NPM_ISOLATED_CACHE_DIRECTORY)));

        final File cacheDirectory = new File(cacheDirectoryPath);
        assertThat("Cache directory should have been created", cacheDirectory, exists());

        // get cache directory for a second time
        assertThat("Should return the same path for multiple calls",
                cacheDirectoryPath, equalTo(NpmTaskType.getTemporaryCacheDirectory(customBuildData)));

        // delete cache directory
        NpmTaskType.deleteTemporaryCacheDirectory(customBuildData);
        assertThat("Should have removed cache directory path from custom build data",
                customBuildData, not(hasKey(BUILD_CTX_NPM_ISOLATED_CACHE_DIRECTORY)));
        assertThat("Should have deleted cache directory", cacheDirectory, not(exists()));
    }

    private Object[] parametersForTestConvertNodePathToNpmPath()
    {
        return new Object[]{
                new Object[]{"/usr/bin/node", "/usr/bin/" + NPM_EXECUTABLE_NAME},
                new Object[]{"/usr/bin/nodejs", "/usr/bin/" + NPM_EXECUTABLE_NAME},
                new Object[]{"/opt/node-v0.10/bin/node", "/opt/node-v0.10/bin/" + NPM_EXECUTABLE_NAME},
                new Object[]{"/opt/node-v0.10/bin/node.js", "/opt/node-v0.10/bin/" + NPM_EXECUTABLE_NAME},
                new Object[]{"~/node", "~/" + NPM_EXECUTABLE_NAME},
                new Object[]{"node", NPM_EXECUTABLE_NAME},
                new Object[]{"C:\\Program Files\\Node.js\\node.exe", "C:\\Program Files\\Node.js\\" + NPM_EXECUTABLE_NAME}
        };
    }
}