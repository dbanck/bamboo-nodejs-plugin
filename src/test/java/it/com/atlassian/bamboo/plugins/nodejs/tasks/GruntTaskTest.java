package it.com.atlassian.bamboo.plugins.nodejs.tasks;

import com.atlassian.bamboo.pageobjects.pages.plan.configuration.ArtifactConfigurationPage;
import com.atlassian.bamboo.pageobjects.pages.plan.configuration.JobTaskConfigurationPage;
import it.com.atlassian.bamboo.plugins.nodejs.pageobjects.GruntTaskComponent;
import it.com.atlassian.bamboo.plugins.nodejs.pageobjects.NpmTaskComponent;
import com.atlassian.bamboo.plugins.nodejs.tasks.grunt.GruntConfigurator;
import com.atlassian.bamboo.plugins.nodejs.tasks.npm.NpmConfigurator;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.testutils.TestBuildDetails;
import com.google.common.collect.ImmutableMap;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import java.io.File;
import java.util.Map;

public class GruntTaskTest extends AbstractNodeTaskTest
{

    @Test
    public void testTask() throws Exception
    {
        final ImmutableMap<String, String> baseGruntTaskConfig = ImmutableMap.of();
        runTaskAndCheckArtifact(baseGruntTaskConfig);
    }

    @Test
    public void testTaskWithAlternativeGruntfile() throws Exception
    {
        final ImmutableMap<String, String> baseGruntTaskConfig = ImmutableMap.of(
                TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY, "src",
                GruntConfigurator.GRUNT_RUNTIME, ".." + File.separator + GruntConfigurator.GRUNT_DEFAULT_EXECUTABLE,
                GruntConfigurator.CONFIG_FILE, ".." + File.separator + "Gruntfile.js");
        runTaskAndCheckArtifact(baseGruntTaskConfig);
    }

    private void runTaskAndCheckArtifact(@NotNull Map<String, String> baseGruntTaskConfig) throws Exception
    {
        final TestBuildDetails plan = createAndSetupPlan();
        final TestBuildDetails defaultJob = plan.getDefaultJob();
        final JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, defaultJob);

        // add npm 'install' task
        final Map<String, String> npmTaskConfig = ImmutableMap.of(NpmConfigurator.COMMAND, "install");
        taskConfigurationPage.addNewTask(NpmTaskComponent.TASK_NAME, NpmTaskComponent.class, "npm install", npmTaskConfig);

        // add Grunt 'less' task
        final Map<String, String> gruntTaskConfig = ImmutableMap.<String, String>builder()
                .putAll(baseGruntTaskConfig)
                .put(GruntConfigurator.TASK, "less")
                .build();

        taskConfigurationPage.addNewTask(GruntTaskComponent.TASK_NAME, GruntTaskComponent.class, "grunt less", gruntTaskConfig);

        // add artifact definition
        final ArtifactConfigurationPage artifactConfigurationPage = product.visit(ArtifactConfigurationPage.class, defaultJob);
        artifactConfigurationPage.createArtifactDefinition("CSS files", "build", "*.css");

        planHelper.runPlanAndWaitForSuccessfulResult(plan.getPlanKey());
    }

}