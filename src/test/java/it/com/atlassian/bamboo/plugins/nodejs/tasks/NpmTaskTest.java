package it.com.atlassian.bamboo.plugins.nodejs.tasks;

import com.atlassian.bamboo.pageobjects.pages.plan.configuration.JobTaskConfigurationPage;
import com.atlassian.bamboo.plugins.nodejs.tasks.npm.NpmConfigurator;
import com.atlassian.bamboo.testutils.TestBuildDetails;
import com.google.common.collect.ImmutableMap;
import it.com.atlassian.bamboo.plugins.nodejs.pageobjects.NpmTaskComponent;
import org.junit.Test;

import java.util.Map;

/**
 * Tests for "npm" Bamboo task.
 * <p/>
 * Only contains non-trivial tests, as basic npm component is used in almost every other test.
 */
public class NpmTaskTest extends AbstractNodeTaskTest
{
    @Test
    public void testTaskWithIsolatedCache() throws Exception
    {
        final TestBuildDetails plan = createAndSetupPlan();
        final JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, plan.getDefaultJob());

        final Map<String, String> npmTaskConfig = ImmutableMap.of(
                NpmConfigurator.COMMAND, "install",
                NpmConfigurator.ISOLATED_CACHE, Boolean.TRUE.toString());

        // just execute the task and make sure it completes without errors
        taskConfigurationPage.addNewTask(NpmTaskComponent.TASK_NAME, NpmTaskComponent.class, "npm install", npmTaskConfig);
        planHelper.runPlanAndWaitForSuccessfulResult(plan.getPlanKey());
    }
}