package it.com.atlassian.bamboo.plugins.nodejs.tasks;

import com.atlassian.bamboo.pageobjects.pages.plan.configuration.ArtifactConfigurationPage;
import com.atlassian.bamboo.pageobjects.pages.plan.configuration.JobTaskConfigurationPage;
import it.com.atlassian.bamboo.plugins.nodejs.pageobjects.NodeTaskComponent;
import com.atlassian.bamboo.plugins.nodejs.tasks.node.NodeConfigurator;
import com.atlassian.bamboo.testutils.TestBuildDetails;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.io.File;
import java.util.Map;

public class NodeTaskTest extends AbstractNodeTaskTest
{

    @Test
    public void testTask() throws Exception
    {
        final TestBuildDetails plan = createAndSetupPlan();
        final TestBuildDetails defaultJob = plan.getDefaultJob();
        final JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, defaultJob);

        // add Node.js task
        final Map<String, String> nodeTaskConfig = ImmutableMap.of(NodeConfigurator.COMMAND, "src" + File.separator + "helloWorld.js");
        taskConfigurationPage.addNewTask(NodeTaskComponent.TASK_NAME, NodeTaskComponent.class, "Hello, World!", nodeTaskConfig);

        // add artifact definition
        final ArtifactConfigurationPage artifactConfigurationPage = product.visit(ArtifactConfigurationPage.class, defaultJob);
        artifactConfigurationPage.createArtifactDefinition("Hello World", "build", "helloWorld.txt");

        planHelper.runPlanAndWaitForSuccessfulResult(plan.getPlanKey());
    }

}