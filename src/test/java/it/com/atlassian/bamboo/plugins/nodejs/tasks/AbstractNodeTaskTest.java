package it.com.atlassian.bamboo.plugins.nodejs.tasks;

import com.atlassian.bamboo.pageobjects.BambooTestedProduct;
import com.atlassian.bamboo.pageobjects.BambooTestedProductFactory;
import com.atlassian.bamboo.pageobjects.helpers.PlanHelper;
import com.atlassian.bamboo.pageobjects.pages.global.BambooDashboardPage;
import com.atlassian.bamboo.pageobjects.pages.plan.configuration.CreatePlanPage;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.testutils.TestBuildDetails;
import com.atlassian.bamboo.testutils.TestBuildDetailsHelper;
import com.atlassian.bamboo.testutils.backdoor.model.Result;
import com.atlassian.bamboo.testutils.config.BambooEnvironmentData;
import com.atlassian.bamboo.testutils.junit.rule.BackdoorRule;
import com.atlassian.bamboo.testutils.vcs.git.GitRepositoryDescriptor;
import com.atlassian.bamboo.testutils.vcs.git.LocalGitSetupHelper;
import com.atlassian.bamboo.webdriver.TestInjectionRule;
import com.atlassian.bamboo.webdriver.WebDriverTestEnvironmentData;
import com.atlassian.plugins.rest.common.util.ReflectionUtils;
import com.atlassian.util.concurrent.LazyReference;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;
import com.google.common.collect.Lists;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;

import javax.inject.Inject;
import java.util.List;


public abstract class AbstractNodeTaskTest
{
    private static final String SUCCESSFUL_TEST_COUNT = "successfulTestCount";
    private static final String FAILED_TEST_COUNT = "failedTestCount";
    private static final String QUARANTINED_TEST_COUNT = "quarantinedTestCount";

    protected static final BambooTestedProduct product = BambooTestedProductFactory.create();
    protected static final LazyReference<GitRepositoryDescriptor> repository = new LazyReference<GitRepositoryDescriptor>()
    {
        @Override
        protected GitRepositoryDescriptor create() throws Exception
        {
            return LocalGitSetupHelper.createRepositoryFromZip("test-repository.zip");
        }
    };

    @Rule public final TestInjectionRule injectionRule = new TestInjectionRule(product);
    @Rule public final BackdoorRule backdoor = new BackdoorRule(new WebDriverTestEnvironmentData());
    @Rule public final WebDriverScreenshotRule screenshotRule = new WebDriverScreenshotRule();

    @Inject
    protected PlanHelper planHelper;

    private final BambooEnvironmentData environmentData = new WebDriverTestEnvironmentData();
    private final List<PlanKey> plansToDelete = Lists.newArrayList();

    @Before
    public void setUp() throws Exception
    {
        backdoor.serverCapabilities().detectServerCapabilities();
    }

    @After
    public void tearDown()
    {
        for (final PlanKey planKey : plansToDelete)
        {
            backdoor.plans().deletePlan(planKey);
        }
    }

    protected TestBuildDetails createAndSetupPlan() throws Exception
    {
        TestBuildDetails plan = TestBuildDetailsHelper.makeUniqueBuild();
        TestBuildDetailsHelper.setupGitPlan(plan, repository.get());
        TestBuildDetailsHelper.setupDefaultJob(plan);
        plan.setManualBuild(true);

        product.gotoLoginPage().loginAsSysAdmin(BambooDashboardPage.class);
        product.visit(CreatePlanPage.class).createNewPlan(plan);
        plansToDelete.add(plan.getPlanKey());

        return plan;
    }

    protected int getTotalNumberOfTestsForBuild(@NotNull PlanKey planKey, int buildNumber) throws Exception
    {
        try
        {
            final Result buildResult = backdoor.plans().getBuildResult(planKey, buildNumber);

            // sadly there are no getters for those fields for now (Bamboo 5.6.0), but they should be added soon (in 5.6.1 or, in worst case, 5.7)
            final int successfulTestCount = (Integer) ReflectionUtils.getFieldValue(Result.class.getDeclaredField(SUCCESSFUL_TEST_COUNT), buildResult);
            final int failedTestCount = (Integer) ReflectionUtils.getFieldValue(Result.class.getDeclaredField(FAILED_TEST_COUNT), buildResult);
            final int quarantinedTestCount = (Integer) ReflectionUtils.getFieldValue(Result.class.getDeclaredField(QUARANTINED_TEST_COUNT), buildResult);

            return successfulTestCount + failedTestCount + quarantinedTestCount;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Couldn't obtain total number of tests for build result", e);
        }
    }

    protected int getNumberOfSuccessfulTestsForBuild(@NotNull PlanKey planKey, int buildNumber) throws Exception
    {
        try
        {
            final Result buildResult = backdoor.plans().getBuildResult(planKey, buildNumber);
            return (Integer) ReflectionUtils.getFieldValue(Result.class.getDeclaredField(SUCCESSFUL_TEST_COUNT), buildResult);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Couldn't obtain number of successful tests for build result", e);
        }
    }
}