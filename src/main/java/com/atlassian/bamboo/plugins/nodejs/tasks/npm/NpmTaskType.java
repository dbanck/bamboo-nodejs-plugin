package com.atlassian.bamboo.plugins.nodejs.tasks.npm;

import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.deployments.execution.DeploymentTaskContext;
import com.atlassian.bamboo.plugins.nodejs.tasks.node.NodeCapabilityDefaultsHelper;
import com.atlassian.bamboo.process.CommandlineStringUtils;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.process.ExternalProcessBuilder;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.CommonTaskType;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.util.BambooFileUtils;
import com.atlassian.bamboo.v2.build.CommonContext;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class NpmTaskType implements CommonTaskType
{
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String BUILD_CTX_NPM_ISOLATED_CACHE_DIRECTORY = "npm.isolatedCache.directory";

    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    private final ProcessService processService;
    private final EnvironmentVariableAccessor environmentVariableAccessor;
    private final CapabilityContext capabilityContext;
    private final I18nResolver i18nResolver;
    // ---------------------------------------------------------------------------------------------------- Constructors

    public NpmTaskType(final ProcessService processService, final EnvironmentVariableAccessor environmentVariableAccessor, final CapabilityContext capabilityContext, I18nResolver i18nResolver)
    {
        this.processService = processService;
        this.environmentVariableAccessor = environmentVariableAccessor;
        this.capabilityContext = capabilityContext;
        this.i18nResolver = i18nResolver;
    }

    // ----------------------------------------------------------------------------------------------- Interface Methods
    @NotNull
    @Override
    public TaskResult execute(@NotNull CommonTaskContext taskContext) throws TaskException
    {
        final Map<String, String> customBuildData = taskContext.getCommonContext().getCurrentResult().getCustomBuildData();
        boolean deleteCacheDirectoryAfterExecution = false;

        try
        {
            final ConfigurationMap configurationMap = taskContext.getConfigurationMap();

            final String nodeRuntime = configurationMap.get(NpmConfigurator.NODE_RUNTIME);
            final String nodePath = capabilityContext.getCapabilityValue(NodeCapabilityDefaultsHelper.NODE_CAPABILITY_PREFIX + "." + nodeRuntime);
            Preconditions.checkState(StringUtils.isNotBlank(nodePath), i18nResolver.getText("node.runtime.error.undefinedPath"));

            final String npmPath = convertNodePathToNpmPath(nodePath);
            final String command = configurationMap.get(NpmConfigurator.COMMAND);
            final Map<String, String> extraEnvironmentVariables = environmentVariableAccessor.splitEnvironmentAssignments(configurationMap.get(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES), false);

            final List<String> arguments = CommandlineStringUtils.tokeniseCommandline(command);
            final ImmutableList.Builder<String> commandListBuilder = ImmutableList.<String>builder().add(npmPath);
            commandListBuilder.addAll(arguments);

            final boolean useIsolatedCache = configurationMap.getAsBoolean(NpmConfigurator.ISOLATED_CACHE);
            if (useIsolatedCache)
            {
                commandListBuilder.add("--cache", getTemporaryCacheDirectory(customBuildData));

                // deployment tasks don't have a custom processor support, so we need to clean the cache dir after each task execution
                if (isDeploymentTask(taskContext))
                {
                    deleteCacheDirectoryAfterExecution = true;
                }
            }

            final TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(taskContext);
            taskResultBuilder.checkReturnCode(processService.executeExternalProcess(taskContext, new ExternalProcessBuilder()
                    .command(commandListBuilder.build())
                    .env(extraEnvironmentVariables)
                    .workingDirectory(taskContext.getWorkingDirectory())));

            return taskResultBuilder.build();
        }
        catch (Exception e)
        {
            throw new TaskException("Failed to execute task", e);
        }
        finally
        {
            if (deleteCacheDirectoryAfterExecution)
            {
                deleteTemporaryCacheDirectory(customBuildData);
            }
        }
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------------------- Helper Methods

    /**
     * A helper method to transform Node.js executable path to npm path.
     * <p/>
     * Note: this solution is ugly, but it works (since usually Node and npm executables are stored in the same
     * directory). Best way would be to create a separate, independent capability for npm, but that would add
     * unnecessary complexity.
     *
     * @param nodePath path to Node.js executable
     *
     * @return a transformed path to npm executable
     */
    @NotNull
    protected static String convertNodePathToNpmPath(@NotNull final String nodePath)
    {
        return FilenameUtils.getFullPath(nodePath) + NodeCapabilityDefaultsHelper.NPM_EXECUTABLE_NAME;
    }

    /**
     * Returns true if this task is being executed in a deployment environment (so it is not executed as a part of a
     * standard build plan).
     *
     * @param commonTaskContext task context
     *
     * @return True, if this task is used in a deployment execution, false otherwise.
     */
    protected static boolean isDeploymentTask(@NotNull CommonTaskContext commonTaskContext)
    {
        return commonTaskContext instanceof DeploymentTaskContext;
    }

    /**
     * Gets an absolute path to a temporary folder for npm isolated cache.
     * <p/>
     * This directory is shared between all npm tasks that are executed within the same {@link CommonContext} (so, for
     * instance, between tasks belonging to one {@link Job}).
     * <p/>
     * During the first call to this method the directory will be created. Next calls within one context will fetch the
     * same directory path.
     * <p/>
     * Call {@link #deleteTemporaryCacheDirectory} to perform a cleanup after the directory is no longer needed.
     *
     * @param customBuildData a map containing custom build data, shared within one {@link CommonContext}
     *
     * @return An absolute path to the npm cache directory.
     *
     * @throws IOException if method fails to create a temporary directory
     */
    @NotNull
    protected static String getTemporaryCacheDirectory(@NotNull Map<String, String> customBuildData) throws IOException
    {
        if (!customBuildData.containsKey(BUILD_CTX_NPM_ISOLATED_CACHE_DIRECTORY))
        {
            final File tempCacheDirectory = BambooFileUtils.createTempDirectory("npm_cache");
            customBuildData.put(BUILD_CTX_NPM_ISOLATED_CACHE_DIRECTORY, tempCacheDirectory.getAbsolutePath());
        }
        return customBuildData.get(BUILD_CTX_NPM_ISOLATED_CACHE_DIRECTORY);
    }

    /**
     * Deletes the temporary npm cache folder and all it's content if it has been created by {@link
     * #getTemporaryCacheDirectory}.
     * <p/>
     * Does nothing if the directory was not created.
     *
     * @param customBuildData a map containing custom build data, shared within one {@link CommonContext}
     */
    protected static void deleteTemporaryCacheDirectory(@NotNull Map<String, String> customBuildData)
    {
        if (customBuildData.containsKey(BUILD_CTX_NPM_ISOLATED_CACHE_DIRECTORY))
        {
            final File tempCacheDirectory = new File(customBuildData.get(BUILD_CTX_NPM_ISOLATED_CACHE_DIRECTORY));
            FileUtils.deleteQuietly(tempCacheDirectory);
            customBuildData.remove(BUILD_CTX_NPM_ISOLATED_CACHE_DIRECTORY);
        }
    }

    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}
