package com.atlassian.bamboo.plugins.nodejs.tasks.mocha.parser;

import com.atlassian.bamboo.build.test.TestCollationService;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import org.jetbrains.annotations.NotNull;

public class MochaParserTaskType implements TaskType
{
    private final TestCollationService testCollationService;

    public MochaParserTaskType(TestCollationService testCollationService)
    {
        this.testCollationService = testCollationService;
    }

    @Override
    @NotNull
    public TaskResult execute(@NotNull TaskContext taskContext) throws TaskException
    {
        final String testFilePattern = taskContext.getConfigurationMap().get(MochaParserConfigurator.PATTERN);
        final String testFilePath = calculateEffectiveFilePattern(taskContext, testFilePattern);
        testCollationService.collateTestResults(taskContext, testFilePath, new MochaReportCollector());
        return TaskResultBuilder.newBuilder(taskContext).checkTestFailures().build();
    }

    /**
     * Calculates the effective file pattern for {@link TestCollationService}. The result will include working directory
     * info from task configuration.
     * <p/>
     * Note: it is necessary to transform the file pattern because methods like {@link
     * TestCollationService#collateTestResults(TaskContext, String)} visit files starting from the build root directory,
     * and not the task working directory.
     * <p/>
     * Link to the issue: https://jira.atlassian.com/browse/BAM-14084
     * <p/>
     * Usage example:
     * <pre>
     *      working directory:        plugin/
     *      file pattern:             *.css
     *      ...
     *      effective file pattern:   plugin/*.css
     * </pre>
     *
     * @param taskContext task context, containing info about build root directory and task working directory.
     * @param filePattern an ant-style file pattern
     *
     * @return A file pattern with task working directory included
     */
    public static String calculateEffectiveFilePattern(@NotNull TaskContext taskContext, @NotNull String filePattern)
    {
        return taskContext.getRootDirectory().toURI().relativize(taskContext.getWorkingDirectory().toURI()).getPath() + filePattern;
    }

}
