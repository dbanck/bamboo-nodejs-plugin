package com.atlassian.bamboo.plugins.nodejs.tasks;

import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plugins.nodejs.tasks.node.NodeCapabilityDefaultsHelper;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.v2.build.agent.capability.Requirement;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementImpl;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

/**
 * Base task configurator for tasks that use Node.js executable.
 */
public abstract class AbstractNodeRequiringTaskConfigurator extends AbstractNodeTaskConfigurator
{
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String NODE_RUNTIME = "runtime";

    protected static final Set<String> FIELDS_TO_COPY = ImmutableSet.<String>builder()
            .addAll(AbstractNodeTaskConfigurator.FIELDS_TO_COPY)
            .add(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES)
            .add(NODE_RUNTIME)
            .build();
    protected static final Map<String, Object> DEFAULT_FIELD_VALUES = ImmutableMap.<String, Object>builder()
            .putAll(AbstractNodeTaskConfigurator.DEFAULT_FIELD_VALUES)
            .build();

    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    @NotNull
    @Override
    public Set<Requirement> calculateRequirements(@NotNull TaskDefinition taskDefinition, @NotNull Job job)
    {
        final String nodeRuntime = taskDefinition.getConfiguration().get(NODE_RUNTIME);
        Preconditions.checkState(StringUtils.isNotBlank(nodeRuntime), i18nResolver.getText("node.runtime.error.empty"));
        return Collections.<Requirement>singleton(new RequirementImpl(NodeCapabilityDefaultsHelper.NODE_CAPABILITY_PREFIX + "." + nodeRuntime, true, ".*"));
    }

    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection)
    {
        super.validate(params, errorCollection);
        if (StringUtils.isEmpty(params.getString(NODE_RUNTIME)))
        {
            errorCollection.addError(NODE_RUNTIME, i18nResolver.getText("node.runtime.error.empty"));
        }
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------------------- Helper Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
    @NotNull
    @Override
    public Set<String> getFieldsToCopy()
    {
        return FIELDS_TO_COPY;
    }

    @NotNull
    @Override
    public Map<String, Object> getDefaultFieldValues()
    {
        return DEFAULT_FIELD_VALUES;
    }
}