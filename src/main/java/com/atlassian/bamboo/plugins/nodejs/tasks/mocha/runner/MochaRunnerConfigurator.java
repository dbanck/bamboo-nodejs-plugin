package com.atlassian.bamboo.plugins.nodejs.tasks.mocha.runner;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plugins.nodejs.tasks.AbstractNodeRequiringTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskTestResultsSupport;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Map;
import java.util.Set;

public class MochaRunnerConfigurator extends AbstractNodeRequiringTaskConfigurator implements TaskTestResultsSupport
{
    // ------------------------------------------------------------------------------------------------------- Constants
    private static final String MOCHA_DEFAULT_EXECUTABLE = Joiner.on(File.separator).join("node_modules", "mocha", "bin", "mocha");

    public static final String MOCHA_RUNTIME = "mochaRuntime";
    public static final String TEST_FILES = "testFiles";
    public static final String PARSE_TEST_RESULTS = "parseTestResults";
    public static final String ARGUMENTS = "arguments";

    protected static final Set<String> FIELDS_TO_COPY = ImmutableSet.<String>builder()
            .addAll(AbstractNodeRequiringTaskConfigurator.FIELDS_TO_COPY)
            .add(MOCHA_RUNTIME)
            .add(TEST_FILES)
            .add(PARSE_TEST_RESULTS)
            .add(ARGUMENTS)
            .build();
    protected static final Map<String, Object> DEFAULT_FIELD_VALUES = ImmutableMap.<String, Object>builder()
            .putAll(AbstractNodeRequiringTaskConfigurator.DEFAULT_FIELD_VALUES)
            .put(MOCHA_RUNTIME, MOCHA_DEFAULT_EXECUTABLE)
            .put(TEST_FILES, "test/")
            .put(PARSE_TEST_RESULTS, true)
            .build();

    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection)
    {
        super.validate(params, errorCollection);
        if (StringUtils.isBlank(params.getString(MOCHA_RUNTIME)))
        {
            errorCollection.addError(MOCHA_RUNTIME, i18nResolver.getText("mocha.runner.runtime.error.empty"));
        }
        if (StringUtils.isEmpty(params.getString(TEST_FILES)))
        {
            errorCollection.addError(TEST_FILES, i18nResolver.getText("mocha.runner.testFiles.error.empty"));
        }
    }

    @Override
    public boolean taskProducesTestResults(@NotNull final TaskDefinition taskDefinition)
    {
        return Boolean.parseBoolean(taskDefinition.getConfiguration().get(PARSE_TEST_RESULTS));
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------------------- Helper Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
    @NotNull
    @Override
    public Set<String> getFieldsToCopy()
    {
        return FIELDS_TO_COPY;
    }

    @NotNull
    @Override
    public Map<String, Object> getDefaultFieldValues()
    {
        return DEFAULT_FIELD_VALUES;
    }
}
