package com.atlassian.bamboo.plugins.nodejs.tasks;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.BuildTaskRequirementSupport;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.collect.ImmutableSet;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

/**
 * Base task configurator for all Node.js plugin Bamboo tasks.
 */
public abstract class AbstractNodeTaskConfigurator extends AbstractTaskConfigurator implements BuildTaskRequirementSupport
{
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = Logger.getLogger(AbstractNodeTaskConfigurator.class);
    // ------------------------------------------------------------------------------------------------------- Constants
    private static final String CTX_UI_CONFIG_BEAN = "uiConfigBean";

    protected static final Set<String> FIELDS_TO_COPY = ImmutableSet.of(TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY);
    protected static final Map<String, Object> DEFAULT_FIELD_VALUES = Collections.emptyMap();

    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    protected I18nResolver i18nResolver;
    protected UIConfigSupport uiConfigSupport;

    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull ActionParametersMap params, @Nullable TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> map = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(map, params, getFieldsToCopy());
        return map;
    }

    @Override
    public void populateContextForCreate(@NotNull Map<String, Object> context)
    {
        super.populateContextForCreate(context);
        context.put(CTX_UI_CONFIG_BEAN, uiConfigSupport);
        context.putAll(getDefaultFieldValues());
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);
        context.put(CTX_UI_CONFIG_BEAN, uiConfigSupport);
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, getFieldsToCopy());
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
    public void setI18nResolver(I18nResolver i18nResolver)
    {
        this.i18nResolver = i18nResolver;
    }

    public void setUiConfigSupport(UIConfigSupport uiConfigSupport)
    {
        this.uiConfigSupport = uiConfigSupport;
    }

    @NotNull
    public Set<String> getFieldsToCopy()
    {
        return FIELDS_TO_COPY;
    }

    @NotNull
    public Map<String, Object> getDefaultFieldValues()
    {
        return DEFAULT_FIELD_VALUES;
    }
}