package com.atlassian.bamboo.plugins.nodejs.tasks.npm;

import com.atlassian.bamboo.build.CustomBuildProcessor;
import com.atlassian.bamboo.v2.build.BuildContext;
import org.jetbrains.annotations.NotNull;

/**
 * A custom build processor that cleans up the temporary cache directory used by npm tasks.
 */
public class NpmCacheCleanupProcessor implements CustomBuildProcessor
{
    private BuildContext buildContext;

    @Override
    public void init(@NotNull BuildContext buildContext)
    {
        this.buildContext = buildContext;
    }

    @NotNull
    @Override
    public BuildContext call() throws Exception
    {
        NpmTaskType.deleteTemporaryCacheDirectory(buildContext.getCurrentResult().getCustomBuildData());
        return buildContext;
    }
}