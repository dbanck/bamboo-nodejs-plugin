[#-- @ftlvariable name="uiConfigBean" type="com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport" --]

[#assign addExecutableLink][@ui.displayAddExecutableInline executableKey='node' /][/#assign]
[@s.select cssClass="builderSelectWidget" labelKey='node.runtime' name='runtime'
list=uiConfigBean.getExecutableLabels('node')
extraUtility=addExecutableLink required=true /]

[@s.textfield labelKey='grunt.runtime' name='gruntRuntime' cssClass="long-field" required=true /]

[@s.textfield labelKey='grunt.task' name='task' /]

[@ui.bambooSection titleKey='repository.advanced.option' collapsible=true
isCollapsed=!(configFile?has_content || environmentVariables?has_content || workingSubDirectory?has_content)]
    [@s.textfield labelKey='grunt.configFile' name='configFile' cssClass="long-field" /]
    [@s.textfield labelKey='builder.common.env' name='environmentVariables' cssClass="long-field" /]
    [@s.textfield labelKey='builder.common.sub' name='workingSubDirectory' cssClass="long-field" /]
[/@ui.bambooSection]
