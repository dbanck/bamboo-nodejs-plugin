# Node.js Bamboo Plugin

Plugin for integrating [Node.js][node] with [Atlassian Bamboo][bamboo]. The plugin provides a number of tasks in Bamboo that you can use to create builds for your Node.js project.

[node]: http://nodejs.org/
[bamboo]: http://www.atlassian.com/software/bamboo/overview

## Tasks

The Node.js Bamboo plugin provides the following tasks in Bamboo:

* Node.js
* npm
* Grunt 0.4.x
* Nodeunit
* Mocha Test Runner
* Mocha Test Parser

## Configure dependencies and install modules

Add the following dependencies (or devDependencies) to the `package.json` file in your Node.js project. These are required if you want to use the "Grunt 0.4.x", "Nodeunit" or "Mocha Test Runner" tasks.

* Grunt 0.4.x
    * [grunt](https://npmjs.org/package/grunt) (>= 0.4)
    * [grunt-cli](https://npmjs.org/package/grunt-cli)
    
* Mocha Test Runner
    * [mocha](https://npmjs.org/package/mocha) 
    * [mocha-bamboo-reporter](https://npmjs.org/package/mocha-bamboo-reporter)

* Nodeunit
    * [nodeunit](https://npmjs.org/package/nodeunit)

Your package.json file should look something like this:

    ...
    "devDependencies": {
        ...
        "mocha": ">=1.18",
        "mocha-bamboo-reporter": "*"
        ...
        "nodeunit" : ">=0.8"
    }
    "dependencies": {
        ...
        "grunt": ">=0.4",
        "grunt-cli": ">=0.1"
    }

Install the necessary `node_modules` before executing any of the tasks described above, by adding an "npm" task with the "install" command.

## Using the Mocha tasks in Bamboo

Executing the "Mocha Test Runner" task will create an output file named `mocha.json`.

You can configure the task to parse test results after a successful execution. Alternatively, you can add a "Mocha Test Parser" task to run afterwards to parse the test results.

If you don't do a full checkout on each build, make sure you add a task to delete mocha.json BEFORE the "Mocha Test Runner" task. A simple script task that runs `rm -f mocha.json` should do the trick.

## Using the Nodeunit task in Bamboo

Executing the "Nodeunit" task will create test results in JUnit XML format.

You can configure the task to parse test results after a successful execution. Alternatively, you can add a "JUnit Parser" task afterwards to parse the test results.

## License

Copyright (C) 2013 - 2014 Atlassian Corporation Pty Ltd. Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.